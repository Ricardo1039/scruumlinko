package mx.linko.trainee.persistence.dao;

import java.util.List;
import mx.linko.trainee.persistence.Usuario;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = {"classpath:mx/linko/trainee/persistence/daos.xml"})
public class UsuarioDaoImplTest {
    
    private UsuarioDao userDao;
    
    @Autowired
    public void setUserDao(UsuarioDao userDao) {
        this.userDao = userDao;
    }
    
    @Test @Ignore
    public void testRead() {        
        System.out.println("read");
        String key = "emartinez@linko.mx";
        Usuario result = userDao.read(key);
        System.out.println(result);
    }
    
    @Test @Ignore
    public void testCreate(){
        System.out.println("creando usuario");
        Usuario usuario = new Usuario();
        usuario.setIdCorreo("jantonio.lopezt@gmail.com");
        usuario.setPassword("8910");
        usuario.setApellido("Lopez Torres");
        usuario.setNombre("Jose Antonio");
        usuario.setAdministrador('N');
        usuario.setColaborador('S');
        usuario.setVisor('N');
        userDao.create(usuario);
        System.out.println("usaurio creado: " + usuario.getNombre() + " " +  usuario.getApellido());
    }
    
    @Test @Ignore
    public void testReadAll(){
        List<Usuario> lista = userDao.readAll();
        for(Usuario usuario : lista){
            System.out.println(usuario);
        }
    }
    
    @Test @Ignore
    public void testDelete(){
        String usario = "jantonio.lopezt@gmail.com";
        userDao.delete(usario);
    }
    
    @Test @Ignore
    public void testUpdate(){
        System.out.println("update usuario");
        Usuario usuario = new Usuario();
        usuario.setAdministrador('S');
        usuario.setApellido("Martinez");
        usuario.setColaborador('S');
        usuario.setIdCorreo("emartinez@linko.mx");
        usuario.setNombre("Emanuel");
        usuario.setPassword("12345");
        usuario.setVisor('S');
        userDao.update(usuario);
    }
    
    @Test @Ignore
    public void testReadById(){
        List<Usuario> usuarios = userDao.readById("emartinez@linko");
        System.out.println(usuarios);
    }
}
