package mx.linko.trainee.service;

import mx.linko.trainee.vo.PrincipalVO;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;

public class SecurityServiceTest {
    
    private SecurityService instance;
    
    @Before
    public void setUp() {
        this.instance = new SecurityServiceStub();
    }

    @Test 
    public void testAuthenticateSuccessful() {
        System.out.println("authenticate successful in progress");
        String username = "emartinez@linko.mx";
        String password = "12345";
        PrincipalVO expResult = new PrincipalVO();
        expResult.setIdCorreo("emartinez@linko.mx");
        expResult.setPassword("12345");
        PrincipalVO result = instance.authenticate(username, password);
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test @Ignore
    public void testAuthenticateFail() {
        System.out.println("authenticate fail in progress");
        String username = "emartinez@linko.mx";
        String password = "654321";
        PrincipalVO result = instance.authenticate(username, password);
        assertNull(result);
    }
    
}
