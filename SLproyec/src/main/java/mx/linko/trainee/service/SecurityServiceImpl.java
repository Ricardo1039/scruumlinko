package mx.linko.trainee.service;

import mx.linko.trainee.persistence.Usuario;
import mx.linko.trainee.persistence.dao.UsuarioDao;
import mx.linko.trainee.vo.PrincipalVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Transactional(readOnly = true)
    public PrincipalVO authenticate(String email, String password) {
        Usuario usuario = this.usuarioDao.read(email);
        if (usuario == null) {
            return null;
        }

        if (!password.equals(usuario.getPassword())) {
            return null;
        }

        PrincipalVO principal = new PrincipalVO();
        principal.setIdCorreo(usuario.getIdCorreo());
        principal.setPassword(usuario.getPassword());
        return principal;
    }

    @Autowired
    private UsuarioDao usuarioDao; 
    
}
