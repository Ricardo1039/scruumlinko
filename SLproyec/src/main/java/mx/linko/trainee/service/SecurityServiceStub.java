/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.linko.trainee.service;

import mx.linko.trainee.vo.PrincipalVO;

/**
 *
 * @author Saint
 */
public class SecurityServiceStub implements SecurityService {

    public PrincipalVO authenticate(String username, String password) {
        if ("emartinez@linko.mx".equalsIgnoreCase(username) && "12345".equals(password)) {
            PrincipalVO principal = new PrincipalVO();
            principal.setIdCorreo("emartinez@linko.mx");
            principal.setPassword("12345");
            return principal;
        } else {
            return null;
        }
    }
}
