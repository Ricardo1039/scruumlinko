package mx.linko.trainee.service;

import mx.linko.trainee.vo.PrincipalVO;


public interface SecurityService {
    /**
     * Authenticates an user with his credentials
     * @param email
     * @param password
     * @return a principal instance if authentication is successful. Null if not.
     */

    PrincipalVO authenticate(String email, String password);
}
