package mx.linko.trainee.persistence;

public class Usuario  implements java.io.Serializable {

    private String idCorreo;
    private String password;
    private String nombre;
    private String apellido;
    private Character administrador;
    private Character colaborador;
    private Character visor;

    public Usuario() {
    }

    public Usuario(String idCorreo, String password, String nombre, String apellido, Character administrador, Character colaborador, Character visor) {
        this.idCorreo = idCorreo;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.administrador = administrador;
        this.colaborador = colaborador;
        this.visor = visor;
    }
   
    public String getIdCorreo() {
        return this.idCorreo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Character getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Character administrador) {
        this.administrador = administrador;
    }

    public Character getColaborador() {
        return colaborador;
    }

    public void setColaborador(Character colaborador) {
        this.colaborador = colaborador;
    }

    public Character getVisor() {
        return visor;
    }

    public void setVisor(Character visor) {
        this.visor = visor;
    }
    
    public void setIdCorreo(String idCorreo) {
        this.idCorreo = idCorreo;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idCorreo=" + idCorreo + ", password=" + password + ", nombre=" + nombre + ", apellido=" + apellido + ", administrador=" + administrador + ", colaborador=" + colaborador + ", visor=" + visor + '}';
    }
}


