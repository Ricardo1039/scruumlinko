/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.linko.trainee.persistence.dao;

import java.util.List;
import mx.linko.trainee.persistence.Usuario;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UsuarioDaoImpl implements UsuarioDao {
    private HibernateTemplate hibernateTemplate;
    
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory){
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }
    
    public Usuario read(String key) {
         return  hibernateTemplate.get(Usuario.class, key);
    }
    
    public List<Usuario> readAll(){
    return  hibernateTemplate.loadAll(Usuario.class);
    
    }

    public String create(Usuario entity) {
        hibernateTemplate.saveOrUpdate(entity);
        hibernateTemplate.flush();
        return entity.getIdCorreo();
    }

    public void delete(String key) {
        Usuario usuario = this.read(key);
        hibernateTemplate.delete(usuario);
    }

    public void update(Usuario entity) {
        hibernateTemplate.update(entity);
    }

    public List<Usuario> readById(String key) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Usuario u where str(u.idCorreo) like :idCorreo");
        List<Usuario> usuarios = query.setParameter("idCorreo", key + "%").list();
        return usuarios;
    }

}
