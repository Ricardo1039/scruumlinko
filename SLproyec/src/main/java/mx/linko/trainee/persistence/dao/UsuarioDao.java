package mx.linko.trainee.persistence.dao;

import java.util.List;
import mx.linko.trainee.persistence.Usuario;

public interface UsuarioDao extends Dao<Usuario,String> {
    List<Usuario>readAll();
    List<Usuario>readById(String key);
}
