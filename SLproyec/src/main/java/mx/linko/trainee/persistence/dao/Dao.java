package mx.linko.trainee.persistence.dao;

import java.util.List;

public interface Dao <Entity ,Key>{
    Key create(Entity entity);
    Entity read(Key key);
    void update(Entity entity);
    void delete(Key key);
}
