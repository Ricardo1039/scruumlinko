package mx.linko.trainee.vo;

import java.io.Serializable;

/**
 *
 * @author Saint
 */
public class PrincipalVO implements Serializable{
    private String email;
    private String password;

    public String getIdCorreo() {
        return email;
    }

    public void setIdCorreo(String idCorreo) {
        this.email = idCorreo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PrincipalVO{" + "idCorreo=" + email + ", password=" + password + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 89 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PrincipalVO other = (PrincipalVO) obj;
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }
        return true;
    }
 
}
