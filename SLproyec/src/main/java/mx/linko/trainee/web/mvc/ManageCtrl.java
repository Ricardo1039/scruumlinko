package mx.linko.trainee.web.mvc;


import java.util.List;
import mx.linko.trainee.persistence.Usuario;
import mx.linko.trainee.persistence.dao.UsuarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping ("/administrar")
public class ManageCtrl {
    
    @RequestMapping (method = RequestMethod.GET)
    public String listUsuario(Model model){
        List<Usuario> list = this.usuarioDao.readAll();
        model.addAttribute("usuarios", list);
        return "fragment/manage";
    }
    
    @Autowired
    private UsuarioDao usuarioDao; 
}

