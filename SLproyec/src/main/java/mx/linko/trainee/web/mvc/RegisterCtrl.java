package mx.linko.trainee.web.mvc;

import java.util.List;
import javax.servlet.http.HttpSession;
import mx.linko.trainee.persistence.Usuario;
import mx.linko.trainee.persistence.dao.UsuarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/registro")
public class RegisterCtrl {
    
    @RequestMapping(method = RequestMethod.GET)
    public String nuevoResgistro() {
        return "fragment/register";
    }
    
   
    @RequestMapping(method = RequestMethod.POST)
    public String crearNvoUsuario(
            @RequestParam("nombre") String nombre,
            @RequestParam("apellido") String apellido,
            @RequestParam("correo") String correo,
            @RequestParam("password") String password,
            @RequestParam(required = false, value = "administrador", defaultValue = "") String administrador,
            @RequestParam(required = false, value = "colaborador", defaultValue = "") String colaborador,
            @RequestParam(required = false, value = "visor", defaultValue = "") String visor,
            HttpSession httpSession,
            Model model) {
        char admin, colab, vis;
        Usuario usuario = new Usuario();
        usuario.setApellido(apellido);
        usuario.setIdCorreo(correo);
        usuario.setNombre(nombre);
        usuario.setPassword(password);

        if (administrador.equals("")) {
            admin = 'N';
        } else {
            admin = administrador.charAt(0);
        }

        if (colaborador.equals("")) {
            colab = 'N';
        } else {
            colab = colaborador.charAt(0);
        }

        if (visor.equals("")) {
            vis = 'N';
        } else {
            vis = visor.charAt(0);
        }

        usuario.setAdministrador(admin);
        usuario.setColaborador(colab);
        usuario.setVisor(vis);
        this.usuarioDao.create(usuario);
        List<Usuario> list = this.usuarioDao.readAll();
        model.addAttribute("usuarios", list);
        return "fragment/manage";
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{idCorreo}")
    public String muestraUsaurioEdit(
            @PathVariable("idCorreo") String idCorreo,
            Model model) {
        System.out.println(idCorreo);
        List<Usuario> usuarios = this.usuarioDao.readById(idCorreo);
        Usuario u = usuarios.get(0);
        System.out.println("usuario seleccionado" + u);
        model.addAttribute("usuario", u);
        
        return "fragment/register";
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "delete/{idCorreo}")
    public String borrarUsuario(
            @PathVariable("idCorreo") String idCorreo,
            Model model) {
        System.out.println(idCorreo);
        List<Usuario> usuarios = this.usuarioDao.readById(idCorreo);
        Usuario u = usuarios.get(0);
        String correo = u.getIdCorreo();
        this.usuarioDao.delete(correo);
         List<Usuario> list = this.usuarioDao.readAll();
         model.addAttribute("usuarios", list);
        
        return "fragment/manage";
    }

    @Autowired
    private UsuarioDao usuarioDao;
}
