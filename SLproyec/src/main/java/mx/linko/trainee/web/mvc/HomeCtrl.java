package mx.linko.trainee.web.mvc;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Saint
 */
@Controller
@RequestMapping("/home")
public class HomeCtrl {
    
    @RequestMapping(method = RequestMethod.GET)
    public String home(HttpSession httpSession){
        logger.info("Preparing home...");
        return "view/home";
    }
    
    private static final Logger logger = Logger.getLogger(HomeCtrl.class);
}
