package mx.linko.trainee.web.mvc;

import javax.servlet.http.HttpSession;
import mx.linko.trainee.service.SecurityService;
import mx.linko.trainee.vo.PrincipalVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/security")
public class LoginCtrl {
    @RequestMapping(
        value = "/signin",
        method = RequestMethod.POST)
    public String signin(
            @RequestParam("email") String email, 
            @RequestParam("password") String password,
            HttpSession httpSession,
            Model model){

        logger.info("Signing in...");
        
        if(logger.isDebugEnabled()){
            logger.debug("Using the following credentials: ");


            logger.debug("\t - email: " + email);

            logger.debug("\t - Password: " + password);
        }
        PrincipalVO principal = this.securityService.authenticate(email, password);
        
        if(principal != null){
            logger.trace("Login successful");
            httpSession.setAttribute("principal", principal);
            return "view/homeCtrl";
        }
        logger.trace("Login failed");
        model.addAttribute("msg", "Correo � contrase�a incorrecta");
        
        return "view/home";
    }
    
    @RequestMapping("/logout")
    public String logout(HttpSession httpSession){
        logger.info("Login out");
        httpSession.invalidate();

        return "view/home";
    }
    
    @Autowired
    private SecurityService securityService;    
    private static final Logger logger = Logger.getLogger(LoginCtrl.class); 

  
    

}
