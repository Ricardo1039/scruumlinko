<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<script type="text/javascript" src="<c:url value="/resources/theme/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/theme/js/eliminar.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/theme/js/editar.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/theme/js/form.js"/>"></script>  
<script type="text/javascript" src="<c:url value="/resources/theme/js/registrar.js"/>"></script> 

<div class="container">
    <div class="col-md-6">
        <h2 class="sub-header">Usuarios</h2>
    </div>
        
    <button id="create-user" class="btn btn-sm btn-default">Create new user</button>
        
</div>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nombre de Usuario</th>
                <th>Apellido</th>
                <th>Correo</th>
                <th>Roles de Proyecto</th>                
            </tr>
        </thead>
        
            <c:forEach var="usuario" items="${usuarios}">
                <tr>
                    <td>${usuario.nombre}</td>
                    <td>${usuario.apellido}</td>
                    <td>${usuario.idCorreo}</td>
                    <td>${usuario.administrador},${usuario.colaborador},${usuario.visor}</td>
                    <td><a id="editar" class="editar" href="<c:url value="/mvc/registro/${usuario.idCorreo}"/>">Editar</a></td>
                    <td><a class="eliminar" href="<c:url value="/mvc/registro/delete/${usuario.idCorreo}"/>">Borrar</a></td>
                </tr> 
            </c:forEach>
        
    </table>
</div>
    

