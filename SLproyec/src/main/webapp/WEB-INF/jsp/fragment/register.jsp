<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form class="form-registro" role="form" id="registroform" action="<c:url value="/mvc/registro"/>" method="POST">
    <input type="nombre" name="nombre" class="form-control" placeholder="Nombre" value="${usuario.nombre}" required autofocus>
    <input type="apellido" name="apellido" class="form-control" placeholder="Apellidos" value="${usuario.apellido}" required>
    <input type="correo" name="correo" class="form-control" placeholder="Correo Electronico" value="${usuario.idCorreo}" required>
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    <h3>Roles:</h3>
    <table>
        <tr>
            <td class="td1"><label>Administrador</label></td>
            <td class="td2"><input type="checkbox" name="administrador" value="S"></td>
        </tr>
        <tr>
            <td class="td1"><label>Colaborador</label></td>
            <td class="td2"><input type="checkbox" name="colaborador" value="S"></td>
        </tr>
        <tr>
            <td class="td1"><label>Visor</label></td>
            <td class="td2"><input type="checkbox" name="visor" value="S"></td>
        </tr>
    </table>
    <br>
    <input id="registrar" type="submit" value="registrar"/>
</form>    
  
    
