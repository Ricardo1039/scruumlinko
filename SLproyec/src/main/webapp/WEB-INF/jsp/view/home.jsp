<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/main.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/bootstrap.min.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/signin.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/register.css"/>"/>
        <script type="text/javascript" src="<c:url value="/resources/theme/js/bootstrap.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/theme/js/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"/>"></script>

        <title>Sing in</title>
    </head>
    <body>
        <div id="container">
            <div id="header" align="center">
                <div id="barra" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <!--<span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>-->
                            </button>
                            <a class="navbar-brand" href="#">SCRUM Linko</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content" align="center">
                <jsp:include page="/WEB-INF/jsp/fragment/user.jsp"/>
                <font color="red">
                <h6>${msg}</h6>
                </font>
            </div>
        </div>
    </body>
</html>

