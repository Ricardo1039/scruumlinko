<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="stylesheet" href="<c:url value="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.0/css/bootstrap-combined.min.css"/>" rel="stylesheet">
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/bootstrap.min.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/signin.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/main.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/theme/css/register.css"/>"/>
        <link rel="stylesheet" href=" <c:url value ="/resources/theme/css/bootstrap-theme.min.css"/>"/>
        <script type="text/javascript" src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/theme/js/administrar.js"/>"></script>



        <title>JSP Page</title>
    </head>
    <body>
        <script type="text/javascript">
            var path = '${pageContext.servletContext.contextPath}';
        </script>
        <jsp:useBean 
            id="principal" 
            scope="session" 
            class="mx.linko.trainee.vo.PrincipalVO"/>

        <div id="container">
            <div id="header">
                <div id="barra" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class='navbar-inner nav-collapse' style="height: auto;">
                        <ul class="nav">
                            <li class="active"><a href="#">SCRUM Linko</a></li>
                            <li><a href="#">Cuadros de Mando</a></li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyectos <b class="caret"></b></a></li>
                            <ul class="dropdown-menu">
                                <li><a href="#">Todos Mis Proyectos</a></li>
                                <li><a href="#">Crear Proyecto</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                            <li class="dropdown">            
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Historias de Usuario <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="">Crear Epica</a></li>
                                    <li><a href="">Crear Historias de Usuario</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a id="adminis" href="<c:url value="/mvc/administrar"/>">Administrar</a></li>
                            <li><a href="" align="right">Perfil : ${principal.idCorreo}</a></li>

                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#" align="right">Perfil</a></li>
                            <li>
                                <a id="logout" href="<c:url value="/mvc/security/logout"/>">Log out</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>  
            <div id="content">


            </div> 
        </div>              
    </body>
</html>
